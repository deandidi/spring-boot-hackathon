package com.conygre.finance.financeapp.controllers;

import java.util.Collection;

import com.conygre.finance.financeapp.entities.Trade;
import com.conygre.finance.financeapp.service.TradeService;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;

@RestController
@RequestMapping("/trades")
public class TradeController {
    @Autowired
    private TradeService tradeService;

    @RequestMapping(method = RequestMethod.GET)
    public Collection<Trade> getTrades() {
        return tradeService.getTrades();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Trade getTrade(@PathVariable("id") String id) {
        return tradeService.getTrade(new ObjectId(id));
    }

    @RequestMapping(method = RequestMethod.POST)
    public void addTrade(@RequestBody Trade trade) {
        tradeService.addTrade(trade);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void removeTrade(@PathVariable("id") String id) {
        tradeService.removeTrade(new ObjectId(id));
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<Trade> updateTrade(@RequestBody Trade trade) {
        if (tradeService.updateTrade(trade)) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}