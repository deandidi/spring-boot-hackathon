package com.conygre.finance.financeapp.entities;

import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Trade {
    public static enum TradeStatus {
        CREATED, PENDING, CANCELLED, REJECTED, FILLED, PARTIALLY_FILLED, ERROR
    }

    @Id
    private ObjectId id;
    private Date date;
    private String ticker;
    private double quantity;
    private double requestedPrice;
    private TradeStatus tradeStatus;

    public boolean verify() {
        return !(id == null || date == null || ticker == null || quantity == 0 || requestedPrice == 0 || tradeStatus == null);
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getRequestedPrice() {
        return requestedPrice;
    }

    public void setRequestedPrice(double requestedPrice) {
        this.requestedPrice = requestedPrice;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public TradeStatus getTradeStatus() {
        return tradeStatus;
    }

    public void setTradeStatus(TradeStatus tradeStatus) {
        this.tradeStatus = tradeStatus;
    }
}