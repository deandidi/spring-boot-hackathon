package com.conygre.finance.financeapp.service;

import java.util.Collection;

import com.conygre.finance.financeapp.entities.Trade;

import org.bson.types.ObjectId;

public interface TradeService {

    Trade getTrade(ObjectId id);
    Collection<Trade> getTrades();
    void addTrade(Trade trade);
    void removeTrade(ObjectId id);
    boolean updateTrade(Trade trade);
}