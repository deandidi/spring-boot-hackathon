package com.conygre.finance.financeapp.repo;

import com.conygre.finance.financeapp.entities.Trade;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TradeRepository extends MongoRepository<Trade, ObjectId> {
    
}